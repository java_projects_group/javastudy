package ObjectOrientedProgramming.NestedClasses;

public class Outer {
    private int counter = 0;

    class Inner {
        void Increament() {
            ++counter;
        }
    }
}
