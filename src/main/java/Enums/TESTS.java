package Enums;

class VirtualExtension {
    private String accountId;
    private String extensionId;

    public VirtualExtension(String accountId, String extensionId) {
        this.accountId = accountId;
        this.extensionId = extensionId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getId() {
        return extensionId;
    }

    public void setId(String extensionId) {
        this.extensionId = extensionId;
    }
}

enum SubscribedEventType {
    CSN("csn") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId() + "/telephony/sessions";
        }
    },
    PRESENCE("presence") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId() + "/presence?detailedTelephonyState=true&sipData=true";
        }
    },
    MISSED_CALL("incoming-call") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return  "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId() + "/incoming-call-pickup";
        }
    },
    INCOMING_CALL("missed-call") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId() + "/missed-calls";
        }
    },
    GLIP("glip") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/glip/*";
        }
    },
    MESSAGE_UPDATE("message-update") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId() + "/message-store";
        }
    },
    EXTENSION_CHANGE("extension-change") {
        @Override
        public String createFilter(VirtualExtension extension) {
            return "/restapi/v1.0/account/" + extension.getAccountId() + "/extension/" + extension.getId();
        }
    };

    private final String name;

    SubscribedEventType(String name) {
        this.name = name;
    }

    // public abstract void doWork();
    public abstract String createFilter(VirtualExtension extension);
}


public class TESTS {

    public static void main(String... params) {
        VirtualExtension extension = new  VirtualExtension("1111111", "22222222");
        SubscribedEventType type = SubscribedEventType.CSN;

        System.out.println(type);
        System.out.println(type.createFilter(extension));
    }
}
