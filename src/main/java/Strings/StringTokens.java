package Strings;

import java.util.StringTokenizer;

public class StringTokens {

	public void Tokens1() {
		String str = "I am sample string and will be tokenized on space";
		StringTokenizer defaultTokenizer = new StringTokenizer(str);
		System.out.println("Total number of tokens found : " + defaultTokenizer.countTokens());
		 
		while (defaultTokenizer.hasMoreTokens()){
		    System.out.println(defaultTokenizer.nextToken());
		}
		System.out.println("Total number of tokens found : " + defaultTokenizer.countTokens());	
	}
	
	public void Test2() {
		String url = "https://howtodoinjava.com/java-initerview-ques+tions";
		StringTokenizer multiTokenizer = new StringTokenizer(url, "://.-+");
		while (multiTokenizer.hasMoreTokens())
		{
		    System.out.println(multiTokenizer.nextToken());
		}
	}
	
	
	public void TEST() {
		String s = "He is a very very good boy, isn't he?";
		StringTokenizer tokens = new StringTokenizer(s, "!,?._' @");
		System.out.println(tokens.countTokens());
		while (tokens.hasMoreTokens())
			System.out.println(tokens.nextToken());
	}
	
	public static void main(String[] args) {
		StringTokens tests = new StringTokens();
		
		// tests.Tokens1();
		// tests.Test2();
		tests.TEST();
	}
}
