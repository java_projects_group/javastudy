package LambdaOperations;

import java.util.List;
import java.util.function.Predicate;

public class Pass_Lambda_as_Param {

    protected static int sumWithCondition(List<Integer> numbers, Predicate<Integer> predicate) {
        return numbers.parallelStream()
                .filter(predicate)
                .mapToInt(i -> i)
                .sum();
    }

    protected void Test1() {
        List<Integer> numbers = List.of(1,2,3,4,5,6,7,8,9,10);
        int value = sumWithCondition(numbers, val -> val > 5);
        System.out.println("Result = " + value);
    }

    public static void main(String ... params) {
        Pass_Lambda_as_Param tests = new Pass_Lambda_as_Param();

        tests.Test1();
    }
}
