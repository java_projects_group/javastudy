package Uuid;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Uuid_Tests {

	public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException 
	{
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid);
		
		int variant = uuid.variant();
		System.out.println(variant);
		
		int version = uuid.version();
		System.out.println(version);
		
		// ---------- Salt -------------
		
		MessageDigest salt = MessageDigest.getInstance("SHA-256");
		salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
		
		System.out.println(salt);
	}
}
