/****************************************************************************
* Copyright 2020 (C) Andrey Tokmakov
* RegExTests.java class
*
* @name    : RegExTests.java
* @author  : Tokmakov Andrey
* @version : 1.0
* @since   : Dec 17, 2020
****************************************************************************/

package RegEx;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RomanNumerals {
	private static final Pattern ROMAN = 
			Pattern.compile("^(?=.)M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");
	
	public boolean isRomanNumeral(String s) {
		return ROMAN.matcher(s).matches();
	}
}

class N11S_Tests {

	public void ParseRegressionInfo_das() {
		final String regexp = "^.*\\?ref=([^&]+)&.*$";
		final String urlParam = "https://git.ringcentral.com/api/v4/projects/11931/repository/files/cd%2Fregression-infos%2Fregression_info_das_night_regression.json/raw?ref=develop&private_token=JMEGiauyh_gWcC-hBGbi";
		System.out.println(urlParam);

		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(urlParam);
		matcher.find();

		System.out.println(matcher.group(1));
	}

	public void ParseRegressionInfo_N11s() {
		final String regexp = "^.*\\?ref=([^&]+)&.*$";
		final String urlParam = "https://git.ringcentral.com/api/v4/projects/15328/repository/files/.ci%2Fnightly_regression_pipeline.json/raw?ref=PLA-57657-implement-ci-regression-cycle&";
		System.out.println(urlParam);

		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(urlParam);
		matcher.find();

		System.out.println(matcher.group(1));
	}
}

class VariousTests {
	public void validatePattern() {
		Pattern pattern = Pattern.compile("[AZ[a-z](a-z)");
		System.out.println(pattern);
	}

	public void validateString_Name() {
		/*
		The username consists of  to  characters inclusive.
		If the username consists of less than  or greater than  characters, then it is an invalid username.
		The username can only contain alphanumeric characters and underscores (_).
		Alphanumeric characters describe the character set consisting of:
		 	lowercase characters [a-z],
		 	uppercase characters [A-Z]
		 	and digits [0-9]
		The first character of the username must be an alphabetic character, i.e.,
		either lowercase character  or uppercase character . [A-Z]
		*/
		final String pattern = "^[A-Za-z][A-Za-z0-9_]{7,29}";

		final List<String> names = List.of("Julia", "Samantha",  "Samantha_21",
				"1Samantha", "Samantha?10_2A", "JuliaZ007", "Julia@007", "_Julia007");

		for (String name: names) {

			System.out.println(name + " = " + name.matches(pattern));
		}
	}
}

public class RegExTests {

	public static void main(String[] args)
	{
		/*
		RomanNumerals R = new RomanNumerals();
		
		System.out.println(R.isRomanNumeral("V"));
		System.out.println(R.isRomanNumeral("X"));
		System.out.println(R.isRomanNumeral("11"));
		*/

		// N11S_Tests tests = new N11S_Tests();
		// tests.ParseRegressionInfo_das();
		// tests.ParseRegressionInfo_N11s();

		VariousTests tests = new VariousTests();
		// tests.validatePattern();
		tests.validateString_Name();
	}
}
