package Format;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class NumberFormating {

    public static void MaximumFractionDigits() {
        double d = 123.45678;
        NumberFormat nf = NumberFormat.getInstance();

        System.out.print("Maximum number of characters in the fractional part " + nf.getMaximumFractionDigits() + ": ");
        System.out.println(nf.format(d));

        nf.setMaximumFractionDigits(7);
        System.out.print("Maximum number of characters in the fractional part 7: ");
        System.out.println(nf.format(d));

        nf.setMaximumIntegerDigits(2);
        System.out.print("Maximum number of characters in an integer part 2: ");
        System.out.println(nf.format(d));
    }

    public static void MinimumFractionDigits() {
        double d = 123.45678;
        NumberFormat nf = NumberFormat.getInstance();

        System.out.print("Minimum number of characters in the fractional part " + nf.getMinimumFractionDigits() + ": ");
        System.out.println( );

        nf.setMinimumFractionDigits(7);
        System.out.print("Minimum number of characters in the fractional part 7: ");
        System.out.println(nf.format(d));

        nf.setMinimumIntegerDigits(5);
        System.out.print("Minimum number of characters in the whole part 5: ");
        System.out.println(nf.format(d));
    }

    public static void CurrencyInstance() {
        // Get the currency instance
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        final double payment = 12345.678;

        Locale indiaLocale = new Locale("en", "IN");

        /* Create NumberFormats using Locales */
        NumberFormat us     = NumberFormat.getCurrencyInstance(Locale.US);
        NumberFormat india  = NumberFormat.getCurrencyInstance(indiaLocale);
        NumberFormat china  = NumberFormat.getCurrencyInstance(Locale.CHINA);
        NumberFormat france = NumberFormat.getCurrencyInstance(Locale.FRANCE);

        /* Print output */
        System.out.println("US: "     + us.format(payment));
        System.out.println("India: "  + india.format(payment));
        System.out.println("China: "  + china.format(payment));
        System.out.println("France: " + france.format(payment));
    }

    public static void main(String ... params) {
        // MaximumFractionDigits();
        // MinimumFractionDigits();

        CurrencyInstance();

    }
}
