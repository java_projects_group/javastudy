package Object_Tests;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class RequireNonNullElseGet {

    private static class MyCoder {
        private final Charset charset;

        MyCoder(Charset charset) {
            this.charset = Objects.requireNonNullElse(charset, StandardCharsets.ISO_8859_1);
        }

        public Charset getCharset() {
            return this.charset;
        }
    }

    private static class MyCoder_Lazy {
        private final Charset charset;

        MyCoder_Lazy(Charset charset) {
            this.charset = Objects.requireNonNullElseGet(charset, MyCoder_Lazy::defaultCharset_LAZY);
        }

        public Charset getCharset() {
            return this.charset;
        }

        private static Charset defaultCharset_LAZY() {
            System.err.println("defaultCharset_LAZY() called");
            return Charset.defaultCharset();
        }
    }

    private static class MyCoder_Lambda {
        private final Charset charset;

        MyCoder_Lambda(Charset charset) {
            this.charset = Objects.requireNonNullElseGet(charset, () -> StandardCharsets.ISO_8859_1);
        }

        public Charset getCharset() {
            return this.charset;
        }
    }

    public static void main(String[] args) {

        MyCoder coder1 = new MyCoder(null);
        System.out.println(coder1.getCharset());

        MyCoder coder2 = new MyCoder(StandardCharsets.UTF_8);
        System.out.println(coder2.getCharset());



        MyCoder_Lazy coder3 = new MyCoder_Lazy(null);
        System.out.println(coder3.getCharset());

        MyCoder_Lazy coder4 = new MyCoder_Lazy(StandardCharsets.UTF_8);
        System.out.println(coder4.getCharset());



        MyCoder_Lambda coder5 = new MyCoder_Lambda(null);
        System.out.println(coder3.getCharset());

        MyCoder_Lambda coder6 = new MyCoder_Lambda(StandardCharsets.UTF_8);
        System.out.println(coder4.getCharset());
    }
}
