/****************************************************************************
* Copyright 2020 (C) Andrey Tokmakov
* Java bitwise pperations demo
*
* @name      : Bit_Tests.java
* @author    : Tokmakov Andrey
* @version   : 1.0
* @since     : November 01, 2020
****************************************************************************/ 

package BitwiseOperation;

public class Bit_Tests {
	
	public static int set_bit(int orig_val, int bit) {
		return (orig_val |= (1 << (bit)));
	}
	
	public static void main(String[] args) {
		int x = 5;
		x = set_bit(x, 1);
		System.out.println(x);
	}
}
