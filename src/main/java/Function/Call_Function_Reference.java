package Function;

import java.util.HashMap;
import java.util.Map;

@FunctionalInterface
interface SimpleFuncInterface {
    public void doWork();
    // public String toString();
    // public boolean equals(Object o);
}

class Worker implements SimpleFuncInterface {
    public void doWork() {
        System.out.println("Worker::doWork() ==> Doing some work");
    }
}

class Worker1 implements SimpleFuncInterface {
    public void doWork() {
        System.out.println("Worker1::doWork() ==> Doing some work");
    }
}

class Handler {
    private final Map<String, SimpleFuncInterface> workers =
            new HashMap<String, SimpleFuncInterface>();

    public void addTask(String name, SimpleFuncInterface func) {
        workers.put(name, func);
    }

    public void runTask(String name) {
        SimpleFuncInterface task = workers.get(name);
        if (null != task)
            task.doWork();
    }
}

public class Call_Function_Reference {

    public static void main(String ... params) {
        Worker worker = new Worker();
        Worker1 worker1 = new Worker1();

        Handler handler = new Handler();
        handler.addTask("Task1", worker::doWork);
        handler.addTask("Task2", worker1::doWork);

        handler.runTask("Task1");
        handler.runTask("Task2");
        handler.runTask("Task1");
    }
}
